﻿using OutsuranceClient.Helpers;
using OutsuranceClient.Properties;
using OutsuranceDomain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace OutsuranceClient.Helpers
{
    public static class WebRequestHelper
    {
        public static string GenerateOutputToFiles()
        {
            string returnValue;
            string URL = Settings.Default.Url;
            HttpClient client = new HttpClient();

            client.BaseAddress = new Uri(URL);
            try
            {
                HttpResponseMessage response = client.GetAsync(URL).Result;
                if (response.IsSuccessStatusCode)
                {
                    returnValue = Settings.Default.Success;
                }
                else
                {
                    returnValue = Settings.Default.Failed;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return returnValue;
        }
    }
}
