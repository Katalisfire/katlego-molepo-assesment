﻿using OutsuranceClient.Helpers;
using OutsuranceClient.Properties;
using OutsuranceWorkflow.Workflow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutsuranceClient
{
    public class Program
    {
        static void Main(string[] args)
        {
            try
            {
                WebRequestHelper.GenerateOutputToFiles(); //If this line fails comment it(since WebApi isnt deployed) and use one below, 
                //GenerateOutputToFilesWorkflow.GenerateOutputToFiles(); //If above line fails comment it and uncomment this one
            }
            catch(Exception ex)
            {
                throw ex;
            }         
        }
    }
}
