﻿using OutsuranceDomain;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutsuranceDAL.Helpers
{
    public static class FileInfoProcessor
    {
        public static List<People> ProcessFile(string path)
        {
            List<People> pupils = new List<People>();
            try
            {
                pupils = File.ReadAllLines(path)
                            .Skip(1)
                            .Select(v => Insert(v)).ToList();

                return pupils;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static People Insert(string row)
        {
            if (!string.IsNullOrWhiteSpace(row))
            {
                var values = row.Split(',');

                if (values != null && values.Length > 3)
                {
                    People person = new People
                    {
                        FirstName = values[0],
                        LastName = values[1],
                        Address = new Address(values[2])
                    };

                    person.Address.Contact = Convert.ToInt32(values[3]);

                    return person;
                }
                else
                {
                    throw new Exception("Error: Something went wrong with processing of file.");
                }
            }
            else
            {
                throw new Exception("Error: Something went wrong with processing of file.");
            }
        }
    }
}
