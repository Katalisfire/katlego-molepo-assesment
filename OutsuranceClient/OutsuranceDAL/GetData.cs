﻿using OutsuranceDAL.Helpers;
using OutsuranceDomain;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutsuranceDAL
{
    public static class GetData
    {
        public static List<People> GetPeople()
        {
            List<People> people = new List<People>();
            try
            {
                string path = String.Format("{0}\\data.csv", AppDomain.CurrentDomain.BaseDirectory);

                people = FileInfoProcessor.ProcessFile(path);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return people;        
        }
    }
}
