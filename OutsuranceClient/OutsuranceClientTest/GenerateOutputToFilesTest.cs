﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OutsuranceWorkflow.Processes;

namespace OutsuranceClientTest
{
    [TestClass]
    public class GenerateOutputToFilesTest
    {
        [TestMethod]
        public void ArrangeFrequencyOrderTestMethod()
        {
            // Arrange
            var people = new PeopleMockData();
            var list = people.People;

            // Act
            var arrangedFrequency = WriteToFileProcess.ArrangeFrequencyOrder(list);

            // Assert
            Assert.AreEqual("[Brown, 2]", arrangedFrequency[0].ToString());
            Assert.AreEqual("[Clive, 2]", arrangedFrequency[1].ToString());
            Assert.AreEqual("[Graham, 2]", arrangedFrequency[2].ToString());
            Assert.AreEqual("[Howe, 2]", arrangedFrequency[3].ToString());
            Assert.AreEqual("[James, 2]", arrangedFrequency[4].ToString());
            Assert.AreEqual("[Owen, 2]", arrangedFrequency[5].ToString());
            Assert.AreEqual("[Smith, 2]", arrangedFrequency[6].ToString());
            Assert.AreEqual("[Jimmy, 1]", arrangedFrequency[7].ToString());
            Assert.AreEqual("[John, 1]", arrangedFrequency[8].ToString());

        }

        [TestMethod]
        public void ArrangeAddressAlphabeticallyTestMethod()
        {
            // Arrange
            var people = new PeopleMockData();
            var list = people.People;

            // Act
            var arrangedAddress = WriteToFileProcess.ArrangeAddressAlphabetically(list);

            // Assert
            Assert.AreEqual("65 Ambling Way", arrangedAddress[0].Address.Number + " " + arrangedAddress[0].Address.Name);
            Assert.AreEqual("8 Crimson Rd", arrangedAddress[1].Address.Number + " " + arrangedAddress[1].Address.Name);
            Assert.AreEqual("12 Howard St", arrangedAddress[2].Address.Number + " " + arrangedAddress[2].Address.Name);
            Assert.AreEqual("102 Long Lane", arrangedAddress[3].Address.Number + " " + arrangedAddress[3].Address.Name);
            Assert.AreEqual("94 Roland St", arrangedAddress[4].Address.Number + " " + arrangedAddress[4].Address.Name);
            Assert.AreEqual("78 Short Lane", arrangedAddress[5].Address.Number + " " + arrangedAddress[5].Address.Name);
            Assert.AreEqual("82 Stewart St", arrangedAddress[6].Address.Number + " " + arrangedAddress[6].Address.Name);
            Assert.AreEqual("49 Sutherland St", arrangedAddress[7].Address.Number + " " + arrangedAddress[7].Address.Name);
        }
    }
}
