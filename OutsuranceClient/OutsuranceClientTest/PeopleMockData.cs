﻿using OutsuranceDomain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutsuranceClientTest
{
    public class PeopleMockData
    {
        public List<People> People { get; set; }

        public PeopleMockData()
        {
            People = new List<People>();
            People.Add(new People() { FirstName = "Jimmy", LastName = "Smith", Address = new Address("102 Long Lane")  { Contact = 29384857 }});
            People.Add(new People() { FirstName = "Clive", LastName = "Owen",  Address = new Address("65 Ambling Way") { Contact = 31214788 }});
            People.Add(new People() { FirstName = "James", LastName = "Brown", Address = new Address("82 Stewart St")  { Contact = 32114566 } });
            People.Add(new People() { FirstName = "Graham", LastName = "Howe", Address = new Address("12 Howard St")   { Contact = 8766556 } });
            People.Add(new People() { FirstName = "John", LastName = "Howe",   Address = new Address("78 Short Lane")  { Contact = 29384857 } });
            People.Add(new People() { FirstName = "Clive", LastName = "Smith", Address = new Address("49 Sutherland St") { Contact = 31214788 } });
            People.Add(new People() { FirstName = "James", LastName = "Owen",  Address = new Address("8 Crimson Rd") { Contact = 32114566 } });
            People.Add(new People() { FirstName = "Graham", LastName = "Brown", Address = new Address("94 Roland St") { Contact = 8766556 } });
        }
    }
}
