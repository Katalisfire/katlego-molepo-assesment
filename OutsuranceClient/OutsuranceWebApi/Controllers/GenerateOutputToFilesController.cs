﻿using OutsuranceWorkflow.Workflow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace OutsuranceWebApi.Controllers
{
    public class GenerateOutputToFilesController : ApiController
    {
        [HttpGet]
        public void GenerateOutputToFiles()
        {
            try
            {
                GenerateOutputToFilesWorkflow.GenerateOutputToFiles();//Writes to the WebApi Project
            }
            catch (Exception ex)
            {
                throw ex;
            }           
        }
    }
}
