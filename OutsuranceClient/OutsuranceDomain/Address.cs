﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutsuranceDomain
{
    public class Address
    {
        public int Number { get; set; }
        public string Name { get; set; }
        public int Contact { get; set; }

        public Address(string address)
        {
            SetAddress(address);
        }

        public void SetAddress(string address)
        {
            if (!string.IsNullOrWhiteSpace(address))
            {
                var values = address.Split(' ');

                if (values != null && values.Length > 2)
                {
                    Number = Convert.ToInt32(values[0]);
                    Name = $"{values[1]} {values[2]}";
                }
            }
        }
    }
}
