﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutsuranceDomain
{
    public interface IWebRequestHelper
    {
        string GenerateOutputToFiles();
    }
}
