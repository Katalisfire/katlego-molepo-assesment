﻿using OutsuranceDomain;
using OutsuranceWorkflow.Processes;
using OutsuranceWorkflow.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutsuranceWorkflow.Workflow
{
    public static class GenerateOutputToFilesWorkflow
    {
        public static void GenerateOutputToFiles()
        {
            List<People> people = new List<People>();
            try
            {
                people = GetDataService.GetPeople();
                WriteToFileProcess.WriteToFileNameFrequency(people);//Writes to the WebApi Project
                WriteToFileProcess.WriteToFileAddressAlphabetically(people);//Writes to the WebApi Project
            }
            catch (Exception ex)
            {
                throw ex;
            }           
        }
    }
}
