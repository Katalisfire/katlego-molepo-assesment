﻿using OutsuranceDomain;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OutsuranceWorkflow.Processes
{
    public static class WriteToFileProcess
    {
        public static void WriteToFileNameFrequency(List<People> people)
        {
            var arrangedFrequency = ArrangeFrequencyOrder(people);
          
            var path = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "FirstOutput.txt");//Writes to the WebApi Project

            using (StreamWriter file = new StreamWriter(path, false)) 
            {
                foreach (var item in arrangedFrequency)
                {
                    file.WriteLine($"{item.Key}, {item.Value}");
                }
             }              
        }

        public static void WriteToFileAddressAlphabetically(List<People> people)
        {
            var arrangedAddresses = ArrangeAddressAlphabetically(people);

            var filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "SecondOutput.txt");//Writes to the WebApi Project

            using (StreamWriter file = new StreamWriter(filePath, false))
            {
                foreach (var item in arrangedAddresses)
                {
                    file.WriteLine($"{item.Address.Number} {item.Address.Name}");
                }
            }
        }

        public static List<KeyValuePair<string, int>> ArrangeFrequencyOrder(List<People> people)
        {
            Dictionary<string, int> frequency = new Dictionary<string, int>();
            try
            {
                foreach (var person in people)
                {
                    InsertValueKey(frequency, person.FirstName);
                    InsertValueKey(frequency, person.LastName);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return frequency.OrderByDescending(x => x.Value).ThenBy(x => x.Key).ToList();
        }

        public static List<People> ArrangeAddressAlphabetically(List<People> people)
        {
            return people.OrderBy(p => p.Address.Name).ToList();
        }

        private static void InsertValueKey(Dictionary<string, int> frequency, string keyValue)
        {
            try
            {
                if (frequency.ContainsKey(keyValue))
                {
                    int value;
                    frequency.TryGetValue(keyValue, out value);

                    value += 1;

                    frequency.InsertUpdate(keyValue, value);
                }
                else
                {
                    frequency.InsertUpdate(keyValue, 1);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            } 
        }

        private static void InsertUpdate<TKey, TValue>(this IDictionary<TKey, TValue> dictionary, TKey key, TValue value)
        {
            try
            {
                if (dictionary.ContainsKey(key))
                {
                    dictionary[key] = value;
                }
                else
                {
                    dictionary.Add(key, value);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
