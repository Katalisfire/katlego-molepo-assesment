﻿using OutsuranceDomain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OutsuranceDAL;

namespace OutsuranceWorkflow.Services
{
    public static class GetDataService
    {
        public static List<People> GetPeople()
        {
            List<People> people = new List<People>();
            try
            {
                people = GetData.GetPeople();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return people;
        }
    }
}
